//
//  DetailViewController.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import UIKit

class DetailViewController: UIViewController {
    var cityName = ""
    var viewModel = DetailViewModel()
    @IBOutlet weak var weatherImage: MyExtendedImage!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var weatherDescLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var maxTempLbl: UILabel!
    @IBOutlet weak var minTempLbl: UILabel!
    @IBOutlet weak var fellsLikeLbl: UILabel!
    @IBOutlet weak var hummidityLbl: UILabel!
    @IBOutlet weak var pressureLbl: UILabel!
    @IBOutlet weak var windSpeedLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel.bindDetailViewModelToController = {
            if let weatherApiData = self.viewModel.weatherApiData, let weatherData = weatherApiData.weather?[0] {
                var iconID = ""
                iconID = weatherData.icon ?? "50d"
                self.setupDetailPage()
                let icon = weatherData.icon ?? ""
                let imageBaseUrl = "\(AppKeys.imageBaseUrl)\(icon)@2x.png"
                self.weatherImage.loadImageWithUrl(URL(string: imageBaseUrl)!)
                self.viewModel.saveSearchedData(weatherApi: weatherApiData)
                print(iconID)
            }
        }
        
        viewModel.onErrorHandling = { error in
            let ac = UIAlertController(title: "Error", message: "Enter valid city name.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(ac, animated: true)
        }
    }
    
    func setupUI() {
        navigationController?.isNavigationBarHidden = false
        date()
        self.viewModel.getData(cityName: cityName)
    }
    
    func setupDetailPage() {
        if let weatherApiData = self.viewModel.weatherApiData, let weatherData = weatherApiData.weather?[0], let mainData = weatherApiData.main, let windData = weatherApiData.wind {
            locationLbl.text = cityName.capitalized
            weatherDescLbl.text = weatherData.description?.capitalized
            tempLbl.text = String(format: "%.0f", (mainData.temp)!) + "°"
            maxTempLbl.text = String(format: "%.0f", (mainData.tempMax)!) + "°" + " /"
            minTempLbl.text = String(format: "%.0f", (mainData.tempMin)!) + "°"
            fellsLikeLbl.text = "Feels Like: " + String(format: "%.0f", (mainData.feelsLike)!) + "°"
            hummidityLbl.text = "Humidity: " + (String((mainData.humidity)!) + "%")
            pressureLbl.text = "Pressure: " + String((mainData.pressure)!) + " hPa"
            windSpeedLbl.text = "Wind Speed: " + String(format: "%.1f", (windData.speed)!) + " km/h"
        }
    }

    func date() {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        let result = formatter.string(from: currentDate)
        dateLbl.text = result
    }    
}
