//
//  MainViewController.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import Foundation
import UIKit
import CoreLocation

class MainViewController: UIViewController {
    
    var viewModel = MainViewModel()
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var currentLocationLbl: UILabel!
    @IBOutlet weak var locationStackView: UIStackView!
    @IBOutlet weak var weatherImage: MyExtendedImage!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var degreeLbl: UILabel!
    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        
        self.viewModel.bindMainViewModelToController = {
            if let weatherApiData = self.viewModel.weatherApiData, let weatherData = weatherApiData.weather?[0], let mainData = weatherApiData.main {
                let icon = weatherData.icon ?? ""
                let imageBaseUrl = "\(AppKeys.imageBaseUrl)\(icon)@2x.png"
                self.weatherImage.loadImageWithUrl(URL(string: imageBaseUrl)!)
                self.degreeLbl.text = String(format: "%.0f", mainData.temp!) + "°"
                self.weatherLbl.text = weatherData.main
                self.cityLbl.text = weatherApiData.name ?? ""
                self.locationStackView.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        viewModel.getLastSearchedLocation()
    }
}

// MARK: - Helper methods
extension MainViewController {
    
    /// Get last searched location saved in user defaults
    func getLastSearchedLocation() -> WeatherApi? {
        let userDefaults = UserDefaults.standard
        if let savedData = userDefaults.object(forKey: AppKeys.WeatherData) as? Data {
            do {
                return try JSONDecoder().decode(WeatherApi.self, from: savedData)
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
}

// MARK: - Textfield delegates
extension MainViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        nameTextField.text = ""
    }
}

// MARK: - IBAction methods
extension MainViewController {
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        if nameTextField.text != "" {
            let vc = DetailViewController()
            guard let name = nameTextField.text else {return}
            vc.cityName = name
            navigationController?.pushViewController(vc, animated: true)
        } else {
            nameTextField.placeholder = "Search field can't be empty"
        }
    }
    
    @IBAction func getMyLocation(_ sender: Any) {
        requestLocationAccess()
    }
}

// MARK: - User's Current Location Handling
extension MainViewController : CLLocationManagerDelegate {
    
    func requestLocationAccess() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    
        if locationManager.authorizationStatus == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(
        _ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation]
    ) {
        if let location = locations.first(where: { $0.horizontalAccuracy <= 50 }) {
            print("location found: \(location)")
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            // Handle location update
            viewModel.getWeatherForAvailableLocation(coordinate: "lat=\(latitude)&lon=\(longitude)")
            currentLocationLbl.isHidden = false
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .notDetermined:
            // Request the appropriate authorization based on the needs of the app
            manager.requestWhenInUseAuthorization()
        case .restricted:
            print("Sorry, restricted")
            // Optional: Offer to take user to app's settings screen
        case .denied:
            print("Sorry, denied")
            // Optional: Offer to take user to app's settings screen
        case .authorizedAlways, .authorizedWhenInUse:
            // The app has permission so start getting location updates
            manager.startUpdatingLocation()
        @unknown default:
            print("Unknown status")
        }
    }
    
    func locationManager(
        _ manager: CLLocationManager,
        didFailWithError error: Error
    ) {
        // Handle failure to get a user’s location
        let ac = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        self.present(ac, animated: true)
    }
}
