//
//  ServiceManager.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import Alamofire

final class ServiceManager {
    public static let shared: ServiceManager = ServiceManager()
}

extension ServiceManager {
    func fetch<T>(path: String, onSuccess: @escaping (T) -> Void, onError: @escaping (Error) -> Void) where T: Codable {
        AF.request(path, encoding: JSONEncoding.default).validate().responseDecodable(of: T.self) { (response) in
            guard let model = response.value else {
                print(response)
                switch response.result {
                case .success(_): break
                case .failure(let error):
                    onError(error)
                }
                return
            }
            onSuccess(model)
        }
    }
}
