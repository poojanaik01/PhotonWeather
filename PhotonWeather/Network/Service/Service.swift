//
//  Service.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import Alamofire

protocol ServiceProtocol {
    func getWeatherData(coordinate: String, onSuccess: @escaping (WeatherApi?) -> Void, onError: @escaping (Error) -> Void)
    func getWeatherData(cityName: String, onSuccess: @escaping (WeatherApi?) -> Void, onError: @escaping (Error) -> Void)
}

final class Service: ServiceProtocol {
    
    /// Get Weather information for User's Location as in Lat & Lon
    /// - Parameter coordinate: user's location coordinate
    func getWeatherData(coordinate: String, onSuccess: @escaping (WeatherApi?) -> Void, onError: @escaping (Error) -> Void) {
        ServiceManager.shared.fetch(path: "\(AppKeys.baseUrl)data/2.5/weather?\(coordinate)&appid=\(AppKeys.appId)&units=metric") { (response: WeatherApi) in
            onSuccess(response)
        } onError: { (error) in
            onError(error)
        }
    }
    
    /// Get Weather information for user's selected city
    /// - Parameter cityName: name of the city
    func getWeatherData(cityName: String, onSuccess: @escaping (WeatherApi?) -> Void, onError: @escaping (Error) -> Void) {
        ServiceManager.shared.fetch(path: "\(AppKeys.baseUrl)data/2.5/weather?q=\(cityName)&appid=\(AppKeys.appId)&units=metric") { (response: WeatherApi) in
            onSuccess(response)
        } onError: { (error) in
            onError(error)
        }
    }
}
