//
//  AppConstants.swift
//  PhotonWeather
//
//  Created by login2 on 06/03/23.
//

import Foundation

struct AppKeys {
    static let WeatherData = "WeatherData"
    static let baseUrl = "https://api.openweathermap.org/"
    static let imageBaseUrl = "http://openweathermap.org/img/wn/"
    static let appId = "3f3bd471de9cce9a1740f7240249de20"
}
