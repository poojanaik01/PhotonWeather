//
//  AppDelegate.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window  =  UIWindow(frame: UIScreen.main.bounds)
        let homePage = MainViewController(nibName: "MainViewController", bundle: nil)
        let navController = UINavigationController(rootViewController: homePage)
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController = navController
        return true
    }
}
