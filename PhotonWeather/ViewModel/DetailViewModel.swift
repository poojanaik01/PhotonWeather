//
//  DetailViewModel.swift
//  PhotonWeather
//
//  Created by login2 on 06/03/23.
//

import Foundation
import UIKit

class DetailViewModel {
    
    private let apiService: ServiceProtocol
    private(set) var weatherApiData : WeatherApi? {
        didSet {
            self.bindDetailViewModelToController()
        }
    }
    
    var onErrorHandling : ((Error) -> Void)?
    var bindDetailViewModelToController : (() -> ()) = {}
    
    init(apiService: ServiceProtocol = Service()) {
        self.apiService = apiService
    }
    
    func saveSearchedData(weatherApi: WeatherApi) {
        do {
            let userDefaults = UserDefaults.standard
            let encodedData = try JSONEncoder().encode(weatherApi)
            userDefaults.set(encodedData, forKey: AppKeys.WeatherData)
            userDefaults.synchronize()
        } catch {
            print("Saving failed")
        }
    }
    
    func getData(cityName: String) {
        apiService.getWeatherData(cityName: cityName) { result in
            guard let resultData = result else {return}
            self.weatherApiData = resultData
        } onError: { error in
            print(error)
            self.onErrorHandling?(error)
        }
    }
}

