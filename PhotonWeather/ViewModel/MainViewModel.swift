//
//  MainViewModel.swift
//  PhotonWeather
//
//  Created by login2 on 03/03/23.
//

import Foundation
import UIKit

class MainViewModel {
    
    private let apiService: ServiceProtocol
    private(set) var weatherApiData : WeatherApi? {
        didSet {
            self.bindMainViewModelToController()
        }
    }
    
    var bindMainViewModelToController : (() -> ()) = {}
    
    init(apiService: ServiceProtocol = Service()) {
        self.apiService = apiService
    }
    
    func getWeatherForAvailableLocation(coordinate: String) {
        apiService.getWeatherData(coordinate: coordinate) { result in
            guard let resultData = result else {return}
            self.weatherApiData = resultData
        } onError: { error in
            print(error)
        }
    }
    
    func getLastSearchedLocation() {
        let userDefaults = UserDefaults.standard
        if let savedData = userDefaults.object(forKey: AppKeys.WeatherData) as? Data {
            do {
                weatherApiData = try JSONDecoder().decode(WeatherApi.self, from: savedData)
            } catch {
            }
        }
    }
}
