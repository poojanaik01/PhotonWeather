//
//  MockMainViewModel.swift
//  PhotonWeatherTests
//
//  Created by login2 on 07/03/23.
//

import Foundation
@testable import PhotonWeather

class MockWeatherViewModel {
    private let apiService: ServiceProtocol
    private(set) var weatherApiData : WeatherApi? {
        didSet {
            self.bindMainViewModelToController()
        }
    }
    var bindMainViewModelToController : (() -> ()) = {}
    
    init(apiService: ServiceProtocol = MockNetworkHelper()) {
        self.apiService = apiService
    }
    
    internal func mockGetWeatherForAvailableLocation(coordinate: String) {
        self.apiService.getWeatherData(coordinate: coordinate) { response in
            guard let resultData = response else {return}
            self.weatherApiData = resultData
        } onError: { error in
            
        }
    }
}
