//
//  MockNetworkHelper.swift
//  PhotonWeatherTests
//
//  Created by login2 on 07/03/23.
//

import Foundation
@testable import PhotonWeather

class MockNetworkHelper: ServiceProtocol {
    func getWeatherData(coordinate: String, onSuccess: @escaping (PhotonWeather.WeatherApi?) -> Void, onError: @escaping (Error) -> Void) {
        do {
            let data = MockNetworkHelper.loadFromJson(filename: "SingleWeather")
            let decoderObject = JSONDecoder()
            let weatherData = try decoderObject.decode(WeatherApi.self, from: data)
            onSuccess(weatherData)
        } catch {
            
        }
    }
    
    func getWeatherData(cityName: String, onSuccess: @escaping (PhotonWeather.WeatherApi?) -> Void, onError: @escaping (Error) -> Void) {
        do {
            let data = MockNetworkHelper.loadFromJson(filename: "SingleWeather")
            let decoderObject = JSONDecoder()
            let weatherData = try decoderObject.decode(WeatherApi.self, from: data)
            onSuccess(weatherData)
        } catch {
            
        }
    }
    
    static let shared = MockNetworkHelper()
    
    /// Load as Json from file
    /// - Parameter fileName: JSon file to load data from
    /// - Returns: data
    private static func loadFromJson(filename fileName: String) -> Data {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let dataObj = try Data(contentsOf: url)
                return dataObj
            } catch {
                print("error:\(error)")
            }
        }
        return Data()
    }
}
